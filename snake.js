const c = document.getElementById('snake')
const ctx = c.getContext('2d')

var snake = []
var length = 4
let unit = 20
var direction = 'right'
let x = 200
let y = c.height/2 - 20
let moveX = 20
let moveY = 0

function clear() {
    ctx.fillStyle='blue';
    ctx.fillRect(0,0,c.width,c.height);
}

document.addEventListener('keydown', key)
function key(e) {
    if (e.keyCode == 37 && direction !== 'right') {moveX = -20, moveY = 0, direction = 'left'} 
    else if (e.keyCode == 38 && direction !== 'down') {moveX = 0, moveY = -20, direction = 'up'} 
    else if (e.keyCode == 39 && direction !== 'left') {moveX = 20, moveY = 0, direction = 'right'} 
    else if (e.keyCode == 40 && direction !== 'up') {moveX = 0, moveY = 20, direction = 'down'} 
    else return
}

function Food(x, y) {
    this.x = Math.floor(Math.random() * c.width / unit) * unit
    this.y = Math.floor(Math.random() * c.height / unit) * unit
}
var food = new Food

function checkBoundries(head) {
    if (head.x < 0 || head.x >= c.width || head.y < 0 || head.y >= c.height) {
        clearInterval(start)
        alert('game over')
    }
}

function draw() {
    detectCollision()
    clear()

    ctx.fillStyle='pink';
    ctx.fillRect(food.x,food.y,20,20);

    snake.forEach(part => {
        ctx.fillStyle='white';
        ctx.fillRect(part.x,part.y,20,20);
    })
    x += moveX
    y += moveY
    snake.push({x,y})
    
    let head = snake[snake.length-1]
    if (head.x == food.x && head.y == food.y) {
        length++
        food = new Food
    }

    if (snake.length >= length) {
        snake.shift()
    }

    checkBoundries(head)
}

function detectCollision() {
    let head = snake[snake.length-1]

    snake.forEach((section, i) => {
        if (i == snake.length-1) {
            return
        } else if (section.x == head.x && section.y == head.y) {
            clearInterval(start)
            alert('game over')
        }
    })
}

function gameOver() {
    ctx.clearRect(0, 0, ctx.width, ctx.height);
    ctx.fillStyle='red';
    ctx.fillRect(0,0,c.width,c.height);

    snake.forEach(part => {
        ctx.fillStyle='white';
        ctx.fillRect(part.x,part.y,20,20);
    })
}


var start = setInterval(draw, 100)